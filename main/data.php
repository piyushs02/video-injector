<!DOCTYPE html>
<html>
<head>
	<title>VideoAPI</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta http-equiv="refresh" content="1800">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link href="http://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/styles/style.css" media="all">
  <link rel="stylesheet" type="text/css" href="/css/customize.css">
  <style type="text/css"></style>
</head>
<body>
  <div class="container">
    <h4>Hello,<?php echo $_SESSION['Username'];?>
      <a href="/login/logout.php" style="margin-left:50px;font-size:20px;">logout</a><br/>
    </h4>
	  <div class="apis-list">
			<h4>ADD APIS</h4>
			<!-- <button type="button" class="btn btn-default">Default</button> -->
			<button id='fb_site' type="button" class="btn btn-primary">Facebook</button>
			<button type="button" class="btn btn-success">Dailymotion</button>
			<button type="button" class="btn btn-info">Twitter</button>
			<button type="button" class="btn btn-warning">Quora</button>
			<button id='yt_site' type="button" class="btn btn-danger">Youtube</button>
			<!-- <button type="button" class="btn btn-link">Link</button>       -->
		</div>
    <div class="connected-api apis-list">
			<h4>Connected APIS</h4>
      <?php
          $apis = get_connected_apis($_SESSION['uid']);
          foreach ($apis as $api) {
              echo "<button type='button' id=". $api['api_name'] ." class='capitalize btn '>".$api['api_name']."</button>  ";
          }
        ?>
    </div>


    <form action='/put-video.php' method='POST'>
      <h3 style="margin-top:30px;"> POST FROM: </h3>
        <?php include "index.html"; ?>
      <h3> POST TO:</h3>
        <?php include "w3-card-post.html"; ?>


      <!-- The Modal -->
      <?php include "modal.html"; ?>
      <button type="submit" class="btn">Submit</button>
    </form>
  </div>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/scripts/modules/script.js"></script>
<script type="text/javascript" src="/js/scripts/modules/plugins.js"></script>
<script type="text/javascript" src="/js/add_api.js"></script>
<script type="text/javascript" src="/js/mainpage-customize.js"></script>


</body>
</html>