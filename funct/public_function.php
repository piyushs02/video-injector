<?php
	function getEmail($uid){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$query = "SELECT email FROM $login_table WHERE uid = ?";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($uid))){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return $row['email'];
		}
		else{
			return 0;
		}
	}

	function getName($uid){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$query = "SELECT fname,lname FROM $login_table WHERE uid = ?";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($uid))){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$name = $row['fname']." ".$row['lname'];
			return $name;
		}
		else{
			return 0;
		}
	}

	function get_api_entity_id($api){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$query = "SELECT api_entity_id FROM `api_list` WHERE api_name = ?";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($api))){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$api_id = $row['api_entity_id'];
			return $api_id;
		}
		else{
			return 0;
		}	
	}

	function authorised($uid,$api_id){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$query = "SELECT uid FROM $user_auth_api WHERE api_entity_id = ?";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($api_id))){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$du_id = $row['uid'];
			if($du_id == $uid){
				return 1;
			}
		}
		return 0;
	}

	function add_user_auth($uid,$api){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$api_id = get_api_entity_id($api);
		if(!authorised($uid,$api_id))
		{
			$query = "INSERT INTO $user_auth_api VALUES (?,?) ";
			$stmt = $conn->prepare($query);
			if(!$stmt->execute(array($uid,$api_id))){
				echo "Error adding api";
				return 0;
			}
		}
		return 1;
		
	}


	function get_connected_apis($uid){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$query = "SELECT api_list.api_name FROM api_list, $user_auth_api WHERE uid = ? AND api_list.api_entity_id = $user_auth_api.api_entity_id";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($uid))){
			$row = $stmt->fetchAll(PDO::FETCH_ASSOC);
          	return $row;
		}
	}


	function check_user_auth($uid,$site){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$table = "user_token_".$site;

		$query = "SELECT uid FROM $table WHERE uid=?";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($uid))){
          	if($stmt->rowCount() < 1){
          		$query = "DELETE FROM $user_auth_api WHERE uid = ? AND api_entity_id IN (SELECT api_list.api_entity_id FROM api_list WHERE api_name = ?)";
          		$stmt = $conn->prepare($query);
          		if(!$stmt->execute(array($uid,$site))){
          			echo "some error occured";
          		}
          	}
		}
	}















?>