<?php

	if(!isset($_SESSION)){
		session_start();
	}


    $base = $_SERVER['DOCUMENT_ROOT'];
    require_once("$base/constants.php");
    include "$base/login/is_login.php";
    date_default_timezone_set('Asia/Kolkata');


if(islogin() && $_SERVER['REQUEST_METHOD'] == 'POST')
{
	if(!empty($_POST['site']) && !empty($_POST['accnt'])){
		$site = $_POST['site'];
		$email = $_POST['accnt'];

		if($site == 'facebook'){
			require_once "$base/apis/facebook/mysrc/user_function.php";
			$fb_id = get_fb_id();
			$access_token = get_fb_token();

			$graphUrl= "https://graph.facebook.com/$fb_id/videos/uploaded?fields=privacy,source,description,picture&access_token=$access_token";
			$data = file_get_contents($graphUrl);
			$jsonData = json_decode($data);
			$data = array();
			$out = array();
			foreach ($jsonData->data as $key) {
				$privacy = $key->privacy->value;
				$source	 = $key->source;
				if(!empty($source)){
					$picture =  $key->picture;
					$id 	 = $key->id;
					$data['url'] = $source;
					$data['picture'] = $picture;
					array_push($out, $data); 
				}
				
			}
			echo json_encode($out);
		}
		else if($site == 'youtube'){
			require_once "$base/apis/google/mysrc/functions.php";
			$channelId = getChannelId($email);
			$access_token = getAccessToken($email);
			
			$api_url = "https://www.googleapis.com/youtube/v3/search?part=id,snippet&channelId=$channelId&order=date&access_token=$access_token";
			$jsonData = file_get_contents($api_url);
			$data1 = json_decode($jsonData);
			$data = array();
			$out = array();
			foreach ($data1->items as $item) {
				if($item->id->kind == 'youtube#video'){
					$data['url'] = 'https://www.youtube.com/embed/'.$item->id->videoId;
					$data['name'] = $item->snippet->title;
					$data['picture'] = $item->snippet->thumbnails->default->url;
					array_push($out, $data);
				}
			}
			echo json_encode($out);
		}
		else{
			echo json_encode("No Api Found");
		}
		
	}
}
else{
		echo json_encode("not authorised");
}



/*

	    $uid = '912006978912272';
	    $access_token = 'EAADfizuLY20BANXdhmxaMMJs28u4mD808gfuj2V5BdsNlI4gQaqpxeIFmDXdnP7ZCLcKDZBPM44XYVkchPVoO3uz0SC4F1CmKqXwMBrpfv4RpEB3IZCbPKlVX2ILie14fMAvZBkiaf9yPUKqDTOc3oYM4goe6ukZD';
	 */   
?>