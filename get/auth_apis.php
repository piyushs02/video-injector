<?php
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";
		include "$base/login/is_login.php";

		if(islogin()){
			if(isset($_POST['data'])){
				$query = "SELECT api_list.api_name FROM api_list, $user_auth_api WHERE api_list.api_entity_id = $user_auth_api.api_entity_id AND $user_auth_api.uid = ?";
				$stmt = $conn->prepare($query);
				if($stmt->execute(array($_SESSION['uid']))){
					$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
					$out = array();
					foreach ($rows as $row) {
						array_push($out, $row['api_name']);
					}
					echo json_encode($out);
				}
				else{
					echo "some error occured";
				}
			}
			else{
				echo "Not authorised";
			}
		}
		else{
			echo "You are not authorised";
		}
		
?>