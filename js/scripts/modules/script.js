
'use strict';

$(document).ready(function() {

    // Default option
    var option = '<option value="0" selected="selected">Select Option</option>';
    var category = '';
    var sites = new Array();


    $.ajax({
        url: "/get/auth_apis.php",
        data:"data=1",
        dataType:'json',
        type: "POST",
        success:function(data){
            $.each(data,function(index,element){
                var card = element+"-card";
                $("#"+card).css("display","inline-block");
                sites.push(element);

                category += '<option value="' + element + '">  ' + element + '</option>';
            });
            $("#first").append(category);

            $.each(sites,function(i,site){
                $.ajax({
                    url: "/get/auth_apis_accnt.php",
                    data:"site="+site,
                    dataType:'json',
                    type: "POST",
                    success:function(accnt){
                        $.each(accnt,function(j,value){
                            var checkboxs = "<label class='checkbox-inline'><input type='checkbox' name='sites["+ value['site'] +"][]' value='"+ value['email'] +"'><img src="+ value['picture'] +" width=30px height=30px /> "+ value['name'] +"  </label><br/>";
                            $("#"+value['site'] + "-card").append(checkboxs);
                        });    
                    },
                    error:function (){}
                });
            });

        },
        error:function (){}
    });




    // Method to clear dropdowns down to a given level
    var clearDropDown = function(arrayObj, startIndex) {
        $.each(arrayObj, function(index, value) {
            if(index >= startIndex) {
                $(value).html(option);
            }
        });
    };

    // Method to disable dropdowns down to a given level
    var disableDropDown = function(arrayObj, startIndex) {
        $.each(arrayObj, function(index, value) {
            if(index >= startIndex) {
                $(value).attr('disabled', 'disabled');
            }
        });
    };

    // Method to enable dropdowns down to a given level
    var enableDropDown = function(that) {
        that.removeAttr('disabled');
    };

    // Method to generate and append options
    var generateOptions = function(element, selection, limit) {
        var options = '';
        $.ajax({
            url: "/get/auth_apis_accnt.php",
            data:"site="+selection,
            dataType:'json',
            type: "POST",
            success:function(data){
                // $("#loaderIcon1").hide();
                $.each(data,function(index,value){
                    options += '<option value="' + value['email'] + '">  ' + value['name'] +"  [" + value['email'] + '] </option>';
                });
                element.append(options);     
            },
            error:function (){}
        });
    };

    // Select each of the dropdowns
    var firstDropDown = $('#first');
    var secondDropDown = $('#second');

    // Hold selected option
    var firstSelection = '';
    var secondSelection = '';

    // Hold selection
    var selection = '';

    // Selection handler for first level dropdown
    firstDropDown.on('change', function() {

        // Get selected option
        firstSelection = firstDropDown.val();

        // Clear all dropdowns down to the hierarchy
        clearDropDown($('select'), 1);

        // Disable all dropdowns down to the hierarchy
        disableDropDown($('select'), 1);

        // Check current selection
        if(firstSelection === '0') {
            return;
        }

        // Enable second level DropDown
        enableDropDown(secondDropDown);

        // Generate and append options
        selection = firstSelection;
        generateOptions(secondDropDown, selection, 3);
    });

    // Selection handler for Second level dropdown
    secondDropDown.on('change', function() {
        secondSelection = secondDropDown.val();
    
        var site = $('select[id=first]').val();
        var accnt = $('select[id=second]').val();
    
        if(site && accnt!=0){
            document.getElementById('id01').style.display='block';

            $("#id01").css("pointer-events","none");
            
            $("#video-modal").empty();
            $("#video-modal").append('<center><img src="/images/loader.gif" id="loaderIcon" /></center>');
            $("#loaderIcon").show();
            $.ajax({
                url: "/get/get-video-list.php",
                data:"site="+site+"&accnt="+accnt,
                dataType:'json',
                cache:false,
                type: "POST",
                success:function(data){
                    var modal ='';
                    $.each(data,function(index,element){
                        modal = "<div class='w3-card-2 video-view'><label class='checkbox-inline'><input type='checkbox' name='video[]' value='"+element['url']+"'><img src="+element['picture']+" width=50px height=50px />  "+ element['name'] +"</label></div>";
                        $("#video-modal").append(modal);
                    });

                    $("#loaderIcon").hide();
                    $("#id01").css("pointer-events","");
            
                },
                error: function(xhr, status, error) {
                  /*var err = eval("(" + xhr.responseText + ")");
                  alert(err.Message);*/
                  console.log(error);
                },
            });
        }
        
    });

});