<!DOCTYPE html>
<html>
<head><meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
	<title>facebook Token</title>
</head>
<body>

<?php
	ob_clean();
	ob_start();
	date_default_timezone_set('Asia/Kolkata');
	$base = $_SERVER['DOCUMENT_ROOT'];
	include "$base/apis/facebook/mysrc/user_function.php";
	require_once("$base/apis/facebook/mysrc/config.php");
	include "$base/apis/facebook/mysrc/functions.php";
	include "$base/funct/public_function.php";


	if(isset($_GET['fbTrue']) AND isset($_GET['code'])){
	    $token_url = "https://graph.facebook.com/oauth/access_token?client_id=".$config['App_ID']."&redirect_uri=" . urlencode($config['callback_url']) . "&client_secret=".$config['App_Secret']."&code=" . $_GET['code']; 

		$response = file_get_contents($token_url);
		$params = null;
		parse_str($response, $params);

		$long_token_url = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=".$config['App_ID']."&client_secret=". $config['App_Secret'] ."&fb_exchange_token=".$params['access_token'];
		$response = file_get_contents($long_token_url);
		$time = date('Y-m-d H:i:s');
		$params_long = null;
		parse_str($response, $params_long);

		$response = set_token($params_long['access_token'],$params_long['expires'],$time);
		
		if($response == 1){
			add_user_auth($_SESSION['uid'],"facebook");
			header("Location: /");
		}
		else if ($response == 2) {
			$url = 'https://www.facebook.com/logout.php?next=' . urlencode($config['callback_url']) .'&access_token='.$params['access_token'];
		    echo "<script type='text/javascript'>var y = confirm('This facebook account is already linked.');</script>";
			header( "refresh:0;url=$url" );
		}
		else{
			echo "<script type='text/javascript'> alert('some error occured');</script>";
			header("Location: /");
		}
	}
	/*else if(valid_token()){
		echo "Already Authorised";
	}*/
	else {
		$id = $config['App_ID'];
      	$cb = $config['callback_url'];
      	$fb_uri = "https://www.facebook.com/dialog/oauth?client_id=$id&redirect_uri=$cb&scope=email,publish_actions,user_videos";
      	header("Location: $fb_uri");
	}

?>


</body>
</html>