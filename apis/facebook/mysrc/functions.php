<?php
function getHTML($url)
	{
	       $ch = curl_init($url); // initialize curl with given url
	       curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]); // set  useragent
	       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // write the response to a variable
	       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // follow redirects if any
	       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60); // max. seconds to execute
	       curl_setopt($ch, CURLOPT_FAILONERROR, 1); // stop when it encounters an error
	       return @curl_exec($ch);
	}

	function uploadVideo($id,$data,$token){
		$graph_url = "https://graph-video.facebook.com/v2.6/".$id."/videos?access_token=".$token;
		
		$query = $data;
		$options = array('http' => array(
										'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
										'method'  => 'POST',
										'content' => http_build_query($query),
										),
						);
		$context  = stream_context_create($options);
		$result = file_get_contents($graph_url, false, $context);
		return $result;
	}
?>