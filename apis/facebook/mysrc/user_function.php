<?php
	function valid_token(){
		// return 0;
		session_start();
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		date_default_timezone_set('Asia/Kolkata');
		$id = $_SESSION['uid'];
		include "$base/connect/nect.php";
		$query = "SELECT fb_expire,fb_token_time FROM $fb_token_table WHERE uid = ?";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($id))){
			$row = $stmt->rowCount();
			if($row > 0){
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				$t_time = $row['fb_token_time'];
				$expire_seconds = $row['fb_expire'];
				$time_now = date('Y-m-d H:i:s');
				$calculated_seconds = $time_now - $t_time;
				$conn = null;
				if($calculated_seconds < $expire_seconds){
					return 1;
				}
			}
		}
		return 0;
	}


	function fetch_fb_id($token){
		$graph_url = "https://graph.facebook.com/me?fields=id&access_token=" . $token;
		$user = json_decode(file_get_contents($graph_url));
		$id = $user->id;
		return $id;
	}
	function fetch_fb_email($token){
		$graph_url = "https://graph.facebook.com/me?fields=email&access_token=" . $token;
		$user = json_decode(file_get_contents($graph_url));
		$id = $user->email;
		return $id;
	}

	function fetch_fb_picture($id){
		$graph_url = "https://graph.facebook.com/$id/picture?type=large";
		$cr = curl_init($graph_url);
		curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cr, CURLOPT_FOLLOWLOCATION, 1); 
		curl_exec($cr);
		$info = curl_getinfo($cr);
		return $info['url'];
	}

	function fetchFbData($token){
		$graph_url = "https://graph.facebook.com/me?fields=id,email,name&access_token=" . $token;
		$user = json_decode(file_get_contents($graph_url));
		return $user;
	}


	function exist_fb_id($fb_id,$fb_email){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		include "$base/connect/nect.php";

		$q = "SELECT uid FROM $fb_token_table WHERE fb_id = ? AND fb_email = ?";
		$stm = $conn->prepare($q);
		if($stm->execute(array($fb_id,$fb_email))){
			if($stm->rowCount() > 0){
				$row = $stm->fetch(PDO::FETCH_ASSOC);
				return $row['uid'];
			}
			else{
				return 0;
			}
		}
		else{
			return 1;
		}
	}

	function set_token($token,$livetime,$time){
		session_start();
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		$uid = $_SESSION['uid'];
		include "$base/connect/nect.php";
		
		$fbData = fetchFbData($token);
		$fb_id = $fbData->id;
		$fb_email = $fbData->email;
		$fb_name = $fbData->name;
		$fb_picture = fetch_fb_picture($fb_id);
		$exist_id = exist_fb_id($fb_id,$fb_email);


		if(!$exist_id){
					$query = "INSERT INTO $fb_token_table VALUES(?,?,?,?,?,?,?,?)";
					$stmt = $conn->prepare($query);
					if($stmt->execute(array($uid,$fb_id, $fb_name, $fb_picture, $fb_email, $token,$livetime,$time))){
						return 1;
					}
					else{
						return 0;
					}
		}
		else if($exist_id != $uid){
					$query = "DELETE FROM $fb_token_table WHERE fb_email = ?;
					 		   INSERT INTO $fb_token_table VALUES(?,?,?,?,?,?,?,?)";

					$existingName = $fb_name;
					$existingUserEmail = $fb_email;
					$msg = "Hi $existingName \nYour facebook id with email $existingUserEmail is verified By Someone else. If You Verified no need to worry but if you didn't Please Reverify Your email to contibue with our services on this email.\nThank You";
					$subject = "Email Unverified";
					$headers = 'From: noreply@videoinjector.com' . "\r\n" .
								'X-Mailer: PHP/' . phpversion();
					$msg = wordwrap($msg,40);
					mail($existingUserEmail,$subject,$msg,$headers);
					
					$stmt = $conn->prepare($query);
					if($stmt->execute(array($fb_email,$uid, $fb_id, $fb_name, $fb_picture, $fb_email, $token, $livetime, $time))){
						check_user_auth($exist_id,"facebook");
						return 1;
					}
					else{
						return 0;
					}
		}
		else if($exist_id == $uid){
					$query = "UPDATE $fb_token_table SET fb_id = ?, name = ?, display_picture = ?, fb_email = ?, fb_token = ?, fb_expire =?, fb_token_time =? WHERE uid = ? AND fb_email = ?";
					$stmt = $conn->prepare($query);
					if($stmt->execute(array($fb_id, $fb_name, $fb_picture, $fb_email, $token, $livetime, $time, $uid, $fb_email))){
						echo $exist_id." ".$uid;
						return 1;
					}
					else{
						return 0;
					}	
		}
		else{
			return 3;
		}
	}













	function get_fb_id(){
		session_start();
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		include "$base/connect/nect.php";
		$id = $_SESSION['uid'];
		$q = "SELECT fb_id FROM $fb_token_table WHERE uid = ?";
		$stm = $conn->prepare($q);

		if($stm->execute(array($id))){
				$row = $stm->fetch(PDO::FETCH_ASSOC);
				$conn = '';
				return $row['fb_id'];
		}
	}

	function get_fb_token(){
		session_start();
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		include "$base/connect/nect.php";
		$id = $_SESSION['uid'];
		$q = "SELECT fb_token FROM $fb_token_table WHERE uid = ?";
		$stm = $conn->prepare($q);
		if($stm->execute(array($id))){
				$row = $stm->fetch(PDO::FETCH_ASSOC);
				$conn = '';
				return $row['fb_token'];
		}
	}


	function set_video_detail($id,$vid,$fb_vid,$time){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		include "$base/connect/nect.php";

		$q = "UPDATE $user_videos_table SET fb_video_id = ?, fb_upload_time = ? WHERE uid = ? AND video_id = ?";
		$stmt = $conn->prepare($q);
		if($stmt->execute(array($fb_vid, $time, $id, $vid))){
			return 1;
		}
		else{
			return 0;
		}

	}
?>