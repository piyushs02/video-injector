<?php
		
	function exist_token($uid,$email){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";

		$query = "SELECT uid FROM $yt_token_table WHERE yt_email = ?";
		$stmt = $conn->prepare($query);
		if($stmt->execute(array($email))){
			if($stmt->rowCount() > 0){
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if($row['uid'] == $uid){
					return 1;
				}
				else{
					return $row['uid'];
				}
			}
			else{
				return 0;
			}
		}
		else{
			die("connection_aborted");
		}
	}

	function set_gtoken($uid, $token, $userDetails, $channelDetails){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/connect/nect.php";
		include "$base/constants.php";
		include "$base/funct/public_function.php";

		$channelDetails = json_decode($channelDetails);
		$channelId = $channelDetails->items[0]->id;

		$response = json_decode($token);
		$access_token = $response->access_token;
	 	$expiry = $response->expires_in;
	 	$refresh_token = $response->refresh_token;
	 	$time = date('Y-m-d H:i:s');

	 	$userData = json_decode($userDetails);
	 	if (isset($userData->id)) {
		    $googleUserId = $userData->id;
		}
		if (isset($userData->email)) {
		    $googleEmail = $userData->email;
		    $googleEmailParts = explode("@", $googleEmail);
		    $googleUserName = $googleEmailParts[0];
		}
		if (isset($userData->verified_email)) {
		    $googleVerified = $userData->verified_email;
		}
		if (isset($userData->name)) {
		    $googleName = $userData->name;
		}
		if (isset($userData->picture)) {
		    $googlePicture = $userData->picture;
		}

		$exist = exist_token($uid,$googleEmail);
		if($exist){
			if($exist == 1){
				$query = "UPDATE $yt_token_table SET access_token= :access_token , exp_date =:expiry , token_time = :token_time , refresh_token = :refresh_token, channel_id = :channel_id WHERE uid = :uid AND yt_email = :email";
				$stmt = $conn->prepare($query);
				$stmt->bindParam(':uid',$uid);
				$stmt->bindParam(':email',$googleEmail);
				$stmt->bindParam(':access_token',$token);
				$stmt->bindParam(':expiry',$expiry);
				$stmt->bindParam(':refresh_token',$refresh_token);
				$stmt->bindParam(':token_time',$token_time);
				$stmt->bindParam(':channel_id',$channelId);

				if($stmt->execute()){
					return 1;
				}
				else{
					return 0;
				}
			}
			else{
				$query = "DELETE FROM $yt_token_table WHERE yt_email = :email;
						  INSERT INTO $yt_token_table VALUES (:uid,:email,:name,:picture,:access_token,:expiry, :refresh_token, :token_time,:gid , :channel_id) ";

				$existingUserEmail = getEmail($uid);
				$existingName = getName($uid);
				$msg = "Hi $existingName \nYour email $existingUserEmail is verified By Someone else. If You Verified no need to worry but if you didn't Please Reverify Your email to contibue with our services on this email.\nThank You";
				$subject = "Email Unverified";
				$headers = 'From: noreply@videoinjector.com' . "\r\n" .
							'X-Mailer: PHP/' . phpversion();
				$msg = wordwrap($msg,40);

				mail($existingUserEmail,$subject,$msg,$headers);
				$stmt = $conn->prepare($query);
				$stmt->bindParam(':uid',$uid);
				$stmt->bindParam(':email',$googleEmail);
				$stmt->bindParam(':name',$googleName);
				$stmt->bindParam(':picture',$googlePicture);
				$stmt->bindParam(':access_token',$token);
				$stmt->bindParam(':expiry',$expiry);
				$stmt->bindParam(':refresh_token',$refresh_token);
				$stmt->bindParam(':token_time',$token_time);
				$stmt->bindParam(':gid',$googleUserId);
				$stmt->bindParam(':channel_id',$channelId);

				if($stmt->execute()){
					check_user_auth($exist,'youtube');
					return 1;
				}
				else{
					return 0;
				}
			}
			

		}
		else{
			$query = "INSERT INTO $yt_token_table (uid,yt_email,name,display_picture, access_token, exp_date, refresh_token ,token_time, gid, channel_id) VALUES(?,?,?,?,?,?,?,?,?,?)";
			$stmt = $conn->prepare($query);
			if($stmt->execute(array($uid, $googleEmail, $googleName, $googlePicture, $token, $expiry, $refresh_token, $time, $googleUserId ,$channelId))){
				echo "Successfull";
				return 1;
			}
			else{
				return 0;
			}
		}
	}


	function getChannelId($email){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		include "$base/connect/nect.php";

		if(!isset($_SESSION)){
			session_start();
		}
		$uid = $_SESSION['uid'];
		$q = "SELECT channel_id FROM $yt_token_table WHERE uid = ? AND yt_email = ?";
		$stmt = $conn->prepare($q);
		if($stmt->execute(array($uid,$email))){
			if($stmt->rowCount() > 0){
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if(!empty($row['channel_id'])){
					return $row['channel_id'];
				}
				else{
					return ;
				}
			}
			else{
				return ;
			}
		}
		else{
			echo "connection problem";
		}

	}


	function getAccessToken($email){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		include "$base/connect/nect.php";

		if(!isset($_SESSION)){
			session_start();
		}
		$uid = $_SESSION['uid'];
		$q = "SELECT access_token,refresh_token FROM $yt_token_table WHERE uid = ? AND yt_email = ?";
		$stmt = $conn->prepare($q);
		if($stmt->execute(array($uid,$email))){
			if($stmt->rowCount() > 0){
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if(!empty($row['access_token'])){

					$url = 'https://www.googleapis.com/oauth2/v4/token';

					$client = '139370514112-9s1i6t9ucvujk4h82nnfll5g7gpa9l7u.apps.googleusercontent.com';
					$secret = 'OWVyR9tW1R9cveMN2ftuxGO-';
					$refresh = $row['refresh_token'];

					$query = array('client_id'=> $client,'client_secret' => $secret, 'refresh_token' => $refresh, 'grant_type'=>'refresh_token');
					$options = array('http' => array(
													'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
													'method'  => 'POST',
													'content' => http_build_query($query),
													),
									);
					$context  = stream_context_create($options);
					$result = file_get_contents($url, false, $context);

					$data = json_decode($result);
					return $data->access_token;
				}
				else{
					return '';
				}
			}
			else{
				return ;
			}
		}
		else{
			echo "connection problem";
		}

	}

	function set_gvideo_detail($id,$vid,$yt_vid,$time){
		$base = $_SERVER['DOCUMENT_ROOT'];
		include "$base/constants.php";
		include "$base/connect/nect.php";

		$q = "UPDATE $user_videos_table SET yt_video_id = ?, yt_upload_time = ? WHERE uid = ? AND video_id = ?";
		$stmt = $conn->prepare($q);
		if($stmt->execute(array($yt_vid, $time, $id, $vid))){
			return 1;
		}
		else{
			return 0;
		}

	}
?>